package utils;

import org.mindrot.jbcrypt.BCrypt;
import java.lang.String;

public class MyCrypt {

    public static String getSalt() {
        String l_result = BCrypt.gensalt(10);
        return l_result;
    };

    public static String getHash (String pi_password, String pi_salt) {
        String main_salt = "37H3Jbhdfu2*@H*378H*3";
        String l_result = null;

        if ((pi_password != "") && (pi_password != null) && (pi_salt != "") && (pi_salt != null)) {
            l_result = BCrypt.hashpw(pi_password + main_salt, pi_salt);
        }
        return l_result;
    }
}
