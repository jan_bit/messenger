package main.java;

import java.util.Date;


public class User extends org.postgresql.util.PGobject implements Cloneable {
    private int id_user;
    private String login;
    private String email;
    private String u_name;
    private String surname;
    private String patronymic;
    private Date birth_date;
    private int gender;
    private Date date_create;
    private String pwd_hash;
    private String pwd_salt;

    public int getId() {
        return id_user;
    }
    public String getLogin() {
        return login;
    }
    public String getEmail() {
        return email;
    }
    public String getName() {
        return u_name;
    }
    public String getSurname() {
        return surname;
    }
    public String getPatronymic() {
        return patronymic;
    }
    public Date getBirthDate() {
        return birth_date;
    }
    public int getGender() {
        return gender;
    }
    public Date getDateCreate() {
        return date_create;
    }
    public String getPwd_hash() {
        return pwd_hash;
    }
    public String getPwd_salt() {
        return pwd_salt;
    }
    //----------------------------------------------

    public void setPwd_hash (String p_pwd_hash) {
        this.pwd_hash = p_pwd_hash;
    }
    public void setPwd_salt (String p_pwd_salt) {
        this.pwd_salt = p_pwd_salt;
    }


    public User(int id_user,
                String login,
                String email,
                String u_name,
                String surname,
                String patronymic,
                Date birth_date,
                int gender, //1 - men, 2 - woman
                Date date_create,
                String pwd_hash,
                String pwd_salt) {
        this.id_user = id_user;
        this.login = login;
        this.email = email;
        this.u_name = u_name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.birth_date = birth_date;
        this.gender = gender;
        this.date_create = date_create;
        this.pwd_hash = pwd_hash;
        this.pwd_salt = pwd_salt;
    }

    public Object clone() {
        return new User(
                id_user,
                login,
                email,
                u_name,
                surname,
                patronymic,
                birth_date,
                gender,
                date_create,
                pwd_hash,
                pwd_salt);
    }

    public String getValue() {
        return "(" +
                String.valueOf(id_user) + ", " +
                login + ", " +
                email +
                u_name + ", " +
                surname + ", " +
                patronymic + ", " +
                birth_date.toString() + ", " +
                String.valueOf(gender) + ", " +
                date_create.toString() + ", " +
                ")";
    };

    @Override
    public String toString() {
        return (this.u_name + "(" + this.login + ")");
    }
}