package main.java;

import Databases.PostgreSQLClass;
import org.json.JSONObject;
import utils.MyCrypt;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class MainServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        RequestDispatcher dispatcher = request.getRequestDispatcher("index.html");
        if (dispatcher != null) {
            dispatcher.forward(request, response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        StringBuilder jb = new StringBuilder();
        String line = null;
        JSONObject jsonToReturn0;

        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null)
                jb.append(line);
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();

            JSONObject jsonObject = new JSONObject(jb.toString());

            int command = jsonObject.getInt("command");
            int id_user_cur;
            int id_user_to;
            int id_user_from;

            String login;
            String password;
            String pwd_salt;
            String pwd_hash;
            main.java.User user;

            //id_user_to
            try {
                id_user_cur = jsonObject.getInt("id_user_cur");
            }
            catch (Exception e){
                id_user_cur = -1;
            };

            //id_user_to
            try {
                id_user_to = jsonObject.getInt("id_user_to");
            }
            catch (Exception e){
                id_user_to = -1;
            };

            //id_user_from
            try {
                id_user_from = jsonObject.getInt("id_user_from");
            }
            catch (Exception e){
                id_user_from = -1;
            };

            switch (command) {

                case 0: //user_page.js:initData
                    ArrayList<String> tags = PostgreSQLClass.getMsgTags();
                    jsonToReturn0 = new JSONObject();
                    jsonToReturn0.put("answer", "tags_list");
                    jsonToReturn0.put("list", tags.toString());
                    out.println(jsonToReturn0.toString());
                    break;

                case 1: //free

                    break;

                case 2: //free

                    break;

                case 4: //addUser
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
                    String dateInString = jsonObject.getString("birth_day");
                    Date birth_day = formatter.parse(dateInString);
                    Date date_create = new Date();
                    jsonToReturn0 = new JSONObject();

                    if (jsonObject.getString("login").isEmpty() ||
                        jsonObject.getString("email").isEmpty() ||
                        jsonObject.getString("name").isEmpty() ||
                        jsonObject.getString("password").isEmpty() ||
                        jsonObject.getString("birth_day").isEmpty()) {
                        jsonToReturn0.put("answer", "addUserNoData");
                        out.println(jsonToReturn0.toString());
                        break;
                    };

                    password = jsonObject.getString("password");
                    pwd_salt = MyCrypt.getSalt();
                    pwd_hash = MyCrypt.getHash(password, pwd_salt);
                    //System.out.println("main_salt.length = " + main_salt.length());

                    user = new main.java.User(-1,
                            jsonObject.getString("login"),
                            jsonObject.getString("email"),
                            jsonObject.getString("name"),
                            jsonObject.getString("surname"),
                            jsonObject.getString("patronymic"),
                            birth_day,
                            jsonObject.getInt("gender"),
                            date_create,
                            pwd_hash,
                            pwd_salt);

                    if (PostgreSQLClass.addUser(user) == 0) {
                        jsonToReturn0.put("answer", "addUserOk");
                        out.println(jsonToReturn0.toString());
                    }
                    else {
                        jsonToReturn0.put("answer", "AddUserError");
                    }

                    break;

                //try Login
                case 5:
                    jsonToReturn0 = new JSONObject();
                    String l_user_data;
                    if (jsonObject.getString("login").isEmpty() || jsonObject.getString("password").isEmpty()) {
                        jsonToReturn0.put("answer", "LoginFalse");
                        out.println(jsonToReturn0.toString());
                        break;
                    };

                    login = jsonObject.getString("login");
                    password = jsonObject.getString("password");
                    user = new main.java.User(-1,
                            login,
                            "",
                            "",
                            "",
                            "",
                            null,
                            1,
                            null,
                            "",
                            "");

                    Boolean login_result = false;
                    l_user_data = PostgreSQLClass.tryLogin(user);
                    if (l_user_data != null) {

                        String db_hash = user.getPwd_hash();
                        pwd_salt = user.getPwd_salt();
                        pwd_hash = MyCrypt.getHash(password, pwd_salt);
                        //System.out.println("pwd_hash = " + pwd_hash.toString());
                        //System.out.println(" db_hash = " + db_hash.toString());
                        if (pwd_hash.equals(db_hash)) {
                            login_result = true;
                        };
                    };

                    if (login_result.equals(false)) {
                        jsonToReturn0.put("answer", "LoginFalse");
                        out.println(jsonToReturn0.toString());
                    }
                    else {
                        jsonToReturn0.put("answer", "LoginTrue");
                        jsonToReturn0.put("list", l_user_data);
                        out.println(jsonToReturn0.toString());
                    }
                    break;

                case 20: //get user Friends
                    ArrayList<String> friends = PostgreSQLClass.getFriendsList(id_user_cur);
                    jsonToReturn0 = new JSONObject();
                    jsonToReturn0.put("answer", "friends_list");
                    jsonToReturn0.put("list", friends.toString());
                    out.println(jsonToReturn0.toString());
                    break;

                case 21: //send message to user
                    jsonToReturn0 = new JSONObject();
                    Boolean l_sended;

                    if (id_user_to <= 0) {
                        jsonToReturn0.put("answer", "EmptyUserTo");
                        out.println(jsonToReturn0.toString());
                        break;
                    }
                    else if (id_user_from <= 0) {
                        jsonToReturn0.put("answer", "EmptyUserFrom");
                        out.println(jsonToReturn0.toString());
                        break;
                    }
                    else if (jsonObject.getString("message_text").isEmpty()) {
                        jsonToReturn0.put("answer", "EmptyMessage");
                        out.println(jsonToReturn0.toString());
                        break;
                    };
                    //System.out.println("message_text = " + jsonObject.getString("message_text"));
                    l_sended = PostgreSQLClass.sendMessage(
                            id_user_from,
                            id_user_to,
                            jsonObject.getString("message_text"));

                    if (l_sended == true) {
                        jsonToReturn0.put("answer", "SendedTrue");
                        out.println(jsonToReturn0.toString());
                    }
                    else {
                        jsonToReturn0.put("answer", "SendedFalse");
                        out.println(jsonToReturn0.toString());
                    }
                    break;

                case 22: //get messages log
                    int last_message_id;
                    try {
                        last_message_id = jsonObject.getInt("last_message_id");
                    }
                    catch (Exception e){
                        last_message_id = -1;
                    };

                    ArrayList<String> messages_log = PostgreSQLClass.getMessagesLog(id_user_cur, id_user_from, last_message_id);
                    jsonToReturn0 = new JSONObject();
                    jsonToReturn0.put("answer", "messages_log");
                    jsonToReturn0.put("list", messages_log.toString());
                    out.println(jsonToReturn0.toString());
                    break;
                
                default:
                    System.out.println("default switch");
                    jsonToReturn0 = new JSONObject();
                    jsonToReturn0.put("MainServlet", "Not found case");
                    out.println(jsonToReturn0.toString());
                    break;

            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}