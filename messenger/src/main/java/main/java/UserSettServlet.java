package main.java;

import Databases.PostgreSQLClass;
import org.json.JSONObject;
import utils.MyCrypt;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class UserSettServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        RequestDispatcher dispatcher = request.getRequestDispatcher("index.html");
        if (dispatcher != null) {
            dispatcher.forward(request, response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        StringBuilder jb = new StringBuilder();
        String line = null;
        JSONObject jsonToReturn0;

        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null)
                jb.append(line);
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();

            JSONObject jsonObject = new JSONObject(jb.toString());

            int command = jsonObject.getInt("command");
            int id_user;


            //id_user_to
            try {
                id_user = jsonObject.getInt("id_user");
            }
            catch (Exception e){
                id_user = -1;
            };


            switch (command) {

                case 1: //change password
                    String old_pass = jsonObject.getString("old_password");
                    String new_pass = jsonObject.getString("new_password");
                    String db_hash;
                    String db_salt;
                    String old_hash;
                    String new_salt;
                    String new_hash;
                    User user;
                    String l_user_data;
                    jsonToReturn0 = new JSONObject();

                    user = new main.java.User(
                            id_user,
                            "",
                            "",
                            "",
                            "",
                            "",
                            null,
                            1,
                            null,
                            "",
                            "");

                    l_user_data = PostgreSQLClass.tryLogin(user);
                    if (l_user_data != null) {
                        db_hash = user.getPwd_hash();
                        db_salt = user.getPwd_salt();
                        old_hash = MyCrypt.getHash(old_pass, db_salt);

                        if (old_hash.equals(db_hash)) {
                            //Old password is correct
                            new_salt = MyCrypt.getSalt();
                            new_hash = MyCrypt.getHash(new_pass, new_salt);
                            if (PostgreSQLClass.changePassword(id_user, new_hash, new_salt) == true) {
                                jsonToReturn0.put("answer", "ChangePassOk");
                            }
                            else {
                                jsonToReturn0.put("answer", "ChangePassError");
                            }
                        }
                        else {
                            jsonToReturn0.put("answer", "ChangePassOldWrong");
                        };

                    };
                    out.println(jsonToReturn0.toString());
                    break;

                case 2: //

                    break;

                default:
                    System.out.println("default switch");
                    jsonToReturn0 = new JSONObject();
                    jsonToReturn0.put("UserSettServlet", "Not found case");
                    out.println(jsonToReturn0.toString());
                    break;

            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
