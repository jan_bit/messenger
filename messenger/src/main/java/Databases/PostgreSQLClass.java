package Databases;

import main.java.User;

import javax.naming.NamingException;
import java.sql.*;
import java.util.*;


public class PostgreSQLClass {
    private static Connection conn;
    private static Statement stat;
    private static ResultSet rs;
    private final static String msg_id_tag = "<%msg_id%>";
    private final static String msg_text_tag = "<%msg_text%>";
    private final static String msg_type_tag = "<%msg_type%>";
    private final static String msg_read_tag = "<%msg_read%>";
    private final static String div_tag = "<^^^>";
    private final static String end_tag = "<@@@>";

    private static void CloseDB() throws ClassNotFoundException, SQLException {
        conn.close();
    }

    private static void Conn() throws ClassNotFoundException, SQLException, NamingException {
        conn = ConnectionPool.getInstance().getConnection();
        if (conn == null) {
            System.err.println("Failed to establish connection");
        }
    }

    public static ArrayList<String> getMsgTags() throws ClassNotFoundException, NamingException {
        ArrayList<String> tags = new ArrayList<String>();
        try {
            tags.add("name=msg_id, value=" + msg_id_tag + ";");
            tags.add("name=msg_text, value=" + msg_text_tag + ";");
            tags.add("name=msg_type, value=" + msg_type_tag + ";");
            tags.add("name=msg_read, value=" + msg_read_tag + ";");
            tags.add("name=div, value=" + div_tag + ";");
            tags.add("name=end, value=" + end_tag + ";");
        }
        catch (Exception e) {
            System.out.println(e);
        }
        return tags;
    }

    public static String tryLogin(User user) throws ClassNotFoundException, SQLException {
        String l_result = null;
            try {
                Conn();
                stat = conn.createStatement();
                PreparedStatement stat = conn.prepareStatement("select * from scm_mes_main.p_try_login(?, ?)");
                stat.setString(1, user.getLogin());
                stat.setInt(2, user.getId());
                rs = stat.executeQuery();

                while (rs.next()) {
                    user.setPwd_hash(rs.getString("pwd_hash"));
                    user.setPwd_salt(rs.getString("pwd_salt"));

                    l_result = "id_user=" + rs.getInt("id_user") + ";" +
                            "login=" + rs.getString("login") + ";" +
                            "email=" + rs.getString("email") + ";" +
                            "u_name=" + rs.getString("u_name") + ";" +
                            "surname=" + rs.getString("surname") + ";" +
                            "patronymic=" + rs.getString("patronymic") + ";" +
                            "gender=" + rs.getInt("gender") + ";";
                    break;
                }

            } catch (Exception e) {
                l_result = null;
                System.out.println(e);
            } finally {
                    stat.close();
                    CloseDB();
            }
        return l_result;
    }


    public static int addUser(User user) throws ClassNotFoundException, SQLException {
        int res;
        try {
            Conn();
            stat = conn.createStatement();
            java.util.Date utilDate = user.getBirthDate();
            java.sql.Date birth_date = new java.sql.Date(utilDate.getTime());

            CallableStatement proc = conn.prepareCall("{? = call scm_mes_main.p_add_user(?, ?, ?, ?, ?, ?, ?, ?, ?) }");
            proc.registerOutParameter(1, Types.INTEGER);
            proc.setString(2, user.getLogin());
            proc.setString(3, user.getName());
            proc.setString(4, user.getSurname());
            proc.setString(5, user.getPatronymic());
            proc.setDate(6, birth_date);
            proc.setInt(7, user.getGender());
            proc.setString(8, user.getEmail());
            proc.setString(9, user.getPwd_hash());
            proc.setString(10, user.getPwd_salt());
            proc.execute();
            res = proc.getInt(1);
        }
        catch (Exception e) {
            res = 1;
            System.out.println(e);
        }
        finally {
            stat.close();
            CloseDB();
        }
        return res;
    }

    public static ArrayList<String> getFriendsList(int i_user_id) throws ClassNotFoundException, SQLException, NamingException {
        ArrayList<String> friends = new ArrayList<String>();
        try {
            Conn();
            stat = conn.createStatement();
            rs = stat.executeQuery("select f.ID_USER, f.USER_NAME from SCM_MES_MAIN.V_MY_FRIENDS f");  //TODO use id_user of current user for filtering friends list

            while (rs.next()) {
                friends.add("u_id=" + rs.getInt("id_user") + ";" +
                        "u_name=" + rs.getString("user_name") + ";");
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            rs.close();
            stat.close();
            CloseDB();
        }
        return friends;
    }

    public static Boolean sendMessage(int id_user_from, int id_user_to, String message_text) throws ClassNotFoundException, SQLException {
        int res;
        try {
            Conn();
            stat = conn.createStatement();
            CallableStatement proc = conn.prepareCall("{? = call scm_mes_main.f_send_message(?, ?, ?) }");
            proc.registerOutParameter(1, Types.INTEGER);
            proc.setInt(2, id_user_from);
            proc.setInt(3, id_user_to);
            proc.setString(4, message_text);
            proc.execute();
            res = proc.getInt(1);
            proc.close();
        } catch (Exception e) {
            res = 1;
            System.out.println(e);
        }
        finally {
            stat.close();
            CloseDB();
        }
        return (res == 0);
    }

    public static ArrayList<String> getMessagesLog(int i_id_user_to, int i_id_user_from, int last_message_id) throws ClassNotFoundException, SQLException, NamingException {
        ArrayList<String> messages_log = new ArrayList<String>();
        try {
            Calendar cl = new GregorianCalendar();
            cl.setTimeInMillis(System.currentTimeMillis());
            cl.add(Calendar.WEEK_OF_MONTH, -2);
            java.util.Date utilDate = cl.getTime();

            java.sql.Date date_start = new java.sql.Date(utilDate.getTime());

            Conn();
            PreparedStatement stat = conn.prepareStatement("select * from scm_mes_main.f_get_u2u_message_log(?,?,?,?)");
            stat.setInt(1, i_id_user_from);
            stat.setInt(2, i_id_user_to);
            stat.setDate(3, date_start);
            stat.setInt(4, last_message_id);

            rs = stat.executeQuery();

            while (rs.next()) {
                messages_log.add(msg_id_tag + "=" + rs.getInt("msg_id") + div_tag +
                        msg_text_tag + "=" + rs.getString("msg_text") + div_tag +
                        msg_type_tag + "=" + rs.getString("msg_type") + div_tag +
                        msg_read_tag + "=" + rs.getString("readed") + div_tag + end_tag);
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            rs.close();
            stat.close();
            CloseDB();
        }
        return messages_log;
    }

    public static Boolean changePassword (int id_user, String new_hash, String new_salt) throws ClassNotFoundException, SQLException {
        Boolean res;
        try {
            Conn();
            stat = conn.createStatement();
            CallableStatement proc = conn.prepareCall("{? = call scm_mes_main.f_change_password(?, ?, ?) }");
            proc.registerOutParameter(1, Types.INTEGER);
            proc.setInt(2, id_user);
            proc.setString(3, new_hash);
            proc.setString(4, new_salt);
            proc.execute();
            res = (proc.getInt(1) == 0);
            proc.close();
        } catch (Exception e) {
            res = false;
            System.out.println(e);
        }
        finally {
            stat.close();
            CloseDB();
        }
        return res;
    }

}
