var serverHostName = window.location.hostname;
var serverProtocolName = window.location.protocol;
var portName = window.location.port;

if (portName.length == 0) {
    portName = "80";
}

if (serverHostName === "localhost")
{
    serverPath = serverProtocolName + "//" + serverHostName + ":" + portName;
}
else
{
    serverPath = serverProtocolName + "//" + serverHostName;
}

function serverConnectFunc(serverUrl, jsonData) {
     console.log("userSetting.js serverConnectFunc", serverUrl, jsonData);

    $.ajax({
        url: serverUrl + "/userSett",
        type: 'POST',
        data: jsonData,
        dataType: 'json',
        async: true,

        success: function (event) {
            // console.log("user_page.js success: answer", event["answer"]);

            switch (event["answer"])
            {
                case "ok":
                    alert("success");
                    break;

                case "ChangePassOk":
                    // console.log("user_page.js, friends_list:", event["list"]);
                    console.log("ChangePassOk");
                    alert("Пароль успешно измененён");
                    break;

                case "ChangePassError":
                    // console.log("user_page.js, friends_list:", event["list"]);
                    console.log("ChangePassError");

                    break;

                case "ChangePassOldWrong":
                    // console.log("user_page.js, friends_list:", event["list"]);
                    console.log("ChangePassOldWrong");
                    alert("Старый пароль не верен");
                    break;

            }
        },
        error: function (xhr, status, error) {
            console.log("error: function", xhr, status, error);
            alert('user_page.js: ' + error);
        }
    });
}

window.onload=function() {
    if (document.getElementById("change_password") != null) {
        document.getElementById("change_password").addEventListener("click", changePassword);
    };
};

function changePassword()
{
    console.log('function changePassword(): start');

    var jsonData = new Object();
    var id_user = get_cookie("id_user");
    var old_pass = document.getElementById("old_password").value;
    var new_pass = document.getElementById("new_password").value;

    if ((id_user == undefined) || (id_user <= 0)) {
        alert("Ошибка! Пользователь не установлен.");
        return;
    }

    if ((old_pass == undefined) || (old_pass == "")) {
        alert("Введите текущий пароль");
        return;
    }

    if ((new_pass == undefined) || (new_pass == "")) {
        alert("Введите новый пароль");
        return;
    }

    jsonData.command = "1";
    jsonData.id_user = id_user;
    jsonData.old_password = old_pass;
    jsonData.new_password = new_pass;


    console.log('jsonData = ', jsonData.toString());
    serverConnectFunc(serverPath, JSON.stringify(jsonData));
};

function get_cookie(cookie_name)
{
    var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

    if ( results )
        return ( unescape ( results[2] ) );
    else
        return null;
}