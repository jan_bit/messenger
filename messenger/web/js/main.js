
let serverHostName = window.location.hostname;
let serverProtocolName = window.location.protocol;
let portName = window.location.port;
let serverPath;


if (portName.length === 0) {
    portName = "80";
}

if (serverHostName === "localhost")
{
    serverPath = serverProtocolName + "//" + serverHostName + ":" + portName;
}
else
{
    serverPath = serverProtocolName + "//" + serverHostName;
}

function serverConnectFunc(serverUrl, jsonData) {
    console.log("serverUrl: " + serverUrl, "jsonData: " + jsonData);

    $.ajax({
        url: serverUrl + "/do",
        type: 'POST',
        data: jsonData,
        dataType: 'json',
        async: true,

        success: function (event) {
             // document.write("success: function (event): " + event["answer"]);
             // console.log("main.js answer: event[answer] = ", event["answer"]);
             // console.log("main.js answer: event[list] = ", event["list"]);
            let keysList;
            $("#table_names").empty();

            switch (event["answer"])
            {
                case "ok":
                    alert("success");
                    break;

                case "names":
                    keysList = event["list"].replace("[", ""). replace("]", "").split(",");
                    //$("#table_names").empty();

                    keysList.forEach(function(item, i, arr) {
                        $("#table_names").append("" +
                            "<tr>" +
                            "<td>" + item +
                            "</td>" +
                            "<td>" +
                            "<a class='btn' align='right' onclick='showAllNames()'>" +
                            "<img align='center' src='delete.png'>" +
                            "</a>" +
                            "</td>" +
                            "</tr>");
                    });

                    break;

                case "specialities":
                    keysList = event["list"].replace("[", ""). replace("]", "").split(",");
                    //$("#table_names").empty();

                    keysList.forEach(function(item, i, arr) {
                        $("#table_names").append("" +
                            "<tr>" +
                            "<td>" + item +
                            "</td>" +
                            "<td>" +
                            "<a class='btn' align='right' onclick='showAllSpec()'>" +
                            "<img align='center' src='delete.png'>" +
                            "</a>" +
                            "</td>" +
                            "</tr>");
                    });

                    break;

                case "addUserNoData":
                    document.write("Обязательные поля должны быть заполнены");
                    // window.location  = 'addUser.html';
                    break;

                case "addUserOk":
                    // console.log("main.js addUserOk: ", event["salt"], event["err"], event["answer"]);
                    // document.write("Добро пожаловать!");

                    keysList = event["list"].replace("[", ""). replace("]", "").split(",");

                    keysList.forEach(function(item, i, arr) {
                        $("#table_names").append("" +
                            "<tr>" +
                            "<td>" + item +
                            "</td>" +
                            "<td>" +
                            "<a class='btn' align='right' onclick='showAllNames()'>" +
                            "<img align='center' src='delete.png'>" +
                            "</a>" +
                            "</td>" +
                            "</tr>");
                    });
                    window.location = '../user_page.html';
                    break;

                // Login
                case "LoginTrue":
                    //console.log("main.js LoginTrue; event[list]=", event["list"]);
                    //localStorage.setItem('token', '123456abcdef');
                    clear_cookies();
                    try {
                        if (event["list"] !== undefined) {
                            let id_user = getPartString(event["list"], "id_user", ";");
                            if (id_user !== undefined) {
                                set_cookie("id_user", id_user);
                                localStorage.setItem('id_user', id_user);
                                //console.log("set cookie: id_user=" + id_user);
                            };

                            let u_name = getPartString(event["list"], "u_name", ";") + " " + getPartString(event["list"], "surname", ";");
                            if (u_name !== undefined) {
                                set_cookie("u_name", u_name);
                                //console.log("set cookie: u_name=" + u_name);
                            };
                        }
                    }
                    catch (e) {
                        // console.log("keysList = ", keysList, getPartString(keysList, "id_user", ";"));
                        document.cookie = "logged=true";
                        console.log("can't set user data: error=" + e.toString());
                    };

                    window.location = '../user_page.html';

                    break;

                case "LoginFalse":
                    clear_cookies();
                    document.write("Не верный логин или пароль");
                    window.location  = '../index.html';
                    //alert("LoginFalse");
                    break;
            }
        },

        error: function (xhr, status, error) {
            console.log("main.js error: " + error, "xhr: " + xhr, "status: " + status);
        }
    });
}

//export let main_obj = {token:loc_token, user:loc_user};
//export let main_token;
export function get_token() {
    //console.log("main loc_token = ", loc_token);
    let val = "123a";
    val = loc_token.value;
    //val = main_token;

    return val;
}
export function get_user() {
    //return 'somebody';
    console.log("somebody");
}

//Part of string pi_string, after end of pi_val_name to ;
export function getPartString(pi_string, pi_val_name, pi_divider) {
    let i_pos1, i_pos2;
    let s_result;
    s_result = null;

    //console.log("getPartString", pi_string, pi_val_name, pi_divider, pi_string.indexOf(pi_val_name));
    try {
        if ((pi_string !== undefined) && (pi_string.indexOf(pi_val_name) >= 0)) {
            i_pos1 = pi_string.indexOf(pi_val_name) + pi_val_name.length + 1;
            pi_string = pi_string.slice(i_pos1);
            i_pos2 = pi_string.indexOf(pi_divider);
            i_pos2 = (i_pos2 <= 0) ? pi_string.length : i_pos2;
            s_result = pi_string.slice(0, i_pos2);
        }
    }
    finally {
        return s_result;
    }
};

// Add user
window.onload=function() {
    if (document.getElementById("addUser") != null) {
        document.getElementById("addUser").addEventListener("click", addUser);
    };

    if (document.getElementById("tryLogin") != null) {
        document.getElementById("tryLogin").addEventListener("click", tryLogin);
    }
};

function addUser()
{
    let jsonData = new Object();
    let nameGender;
    let resGender = undefined;

    jsonData.command = "4";

    nameGender = document.getElementsByName("InputGender");
    for (let i = 0; i < nameGender.length; i++) {
        if (nameGender[i].selected) {
            resGender = nameGender[i].value;
        }
    };

    if (resGender == undefined) {
        alert('Не выбран пол');
    };

    jsonData.login = document.getElementById("NewLoginInput").value;
    jsonData.email = document.getElementById("NewEmailInput").value;
    jsonData.name = document.getElementById("NewNameInput").value;
    jsonData.surname = document.getElementById("NewSurnameInput").value;
    jsonData.patronymic = document.getElementById("NewPatronymicInput").value;
    jsonData.birth_day = document.getElementById("NewBirthDayInput").value;
    jsonData.gender = resGender;
    jsonData.password = document.getElementById("NewPasswordInput").value;

    serverConnectFunc(serverPath, JSON.stringify(jsonData));
};

// Login
function tryLogin()
{
    console.log('tryLogin');
    let jsonData = new Object();
    jsonData.command = "5";
    jsonData.login = document.getElementById("loginLogin").value;
    jsonData.password = document.getElementById("loginPassword").value;
    serverConnectFunc(serverPath, JSON.stringify(jsonData));
};

function set_cookie (name, value)
{
    let cookie_string = name + "=" + escape ( value );
    document.cookie = cookie_string;
}

export function get_cookie(cookie_name)
{
    let results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

    if ( results )
        return ( unescape ( results[2] ) );
    else
        return null;
}

function delete_cookie(cookie_name)
{
    let cookie_date = new Date ( );  // Текущая дата и время
    cookie_date.setTime ( cookie_date.getTime() - 1 );
    document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString();
}

function clear_cookies() {
    delete_cookie("id_user");
    delete_cookie("u_name");
    delete_cookie("user_data");
}