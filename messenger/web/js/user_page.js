
import {get_cookie} from "./main.js";
import {getPartString} from "./main.js";

let serverHostName = window.location.hostname;
let serverProtocolName = window.location.protocol;
let portName = window.location.port;
let serverPath;
//
let gc_sess = {cur_user_id: null, actual_friend_id: null, last_msg_id: null, msg_count: null};
let gc_msg_tag = {id: null, text: null, type: null, read: null, div: null, end: null};
let keyCtrl, keyEnter;

if (portName.length === 0) {
    portName = "80";
}

if (serverHostName === "localhost")
{
    serverPath = serverProtocolName + "//" + serverHostName + ":" + portName;
}
else
{
    serverPath = serverProtocolName + "//" + serverHostName;
}

function serverConnectFunc(serverUrl, jsonData) {
    // console.log("user_page.js serverConnectFunc", serverUrl, jsonData);

    $.ajax({
        url: serverUrl + "/do",
        type: 'POST',
        data: jsonData,
        dataType: 'json',
        async: true,

        success: function (event) {
             // console.log("user_page.js success: answer", event["answer"]);
            let keysList;
            switch (event["answer"]) {

                case "tags_list":
                    //debugger;
                    keysList = event["list"].replace("[", "").replace("]", "").split(";");
                    let tag_name, tag_value;
                    keysList.forEach(function(item, i, arr) {
                        tag_name = getPartString(item, "name", ",");
                        tag_value = getPartString(item, "value", ",");
                        switch (tag_name) {
                            case "msg_id":
                                gc_msg_tag.id = tag_value;
                                break;
                            case "msg_text":
                                gc_msg_tag.text = tag_value;
                                break;
                            case "msg_type":
                                gc_msg_tag.type = tag_value;
                                break;
                            case "msg_read":
                                gc_msg_tag.read = tag_value;
                                break;
                            case "div":
                                gc_msg_tag.div = tag_value;
                                break;
                            case "end":
                                gc_msg_tag.end = tag_value;
                                break;
                            default:
                                return;
                        }
                    });
                    break;

                case "SendedTrue":
                    showMessagesLog();
                    break;

                case "friends_list":
                    keysList = event["list"].replace("[", "").replace("]", "").split(",");

                    let frl = $("#sel_friends_list");
                    frl.empty();
                    keysList.forEach(function(item, i, arr) {
                        frl.append("" +
                            "<li>" +
                                "<a href=#" +
                                " id=" + getPartString(item, "u_id", ";") +
                                ">" +
                                getPartString(item, "u_name", ";") +
                                "</a>" +
                            "</li>");
                    });
                    break;

                case "messages_log":
                    let t = $("#messages_log");
                    keysList = event["list"].split(gc_msg_tag.end);
                    let msg_text, msg_type, msg_log, last_mes_id = -1;

                    msg_log = '';
                    keysList.forEach(function(item, i, arr) {
                        msg_text = getPartString(item, gc_msg_tag.text, gc_msg_tag.div);
                        //debugger;
                        if (msg_text !== null) {
                            last_mes_id = getPartString(item, gc_msg_tag.id, gc_msg_tag.div);
                            msg_type = getPartString(item, gc_msg_tag.type, gc_msg_tag.div);
                            //console.log("msg_text = ", msg_text, "msg_type=" + msg_type);
                            switch (msg_type) {
                                case "1":
                                    if (gc_sess.msg_count === 0) {//Если текст-сообщения или оно первое... (исключаем постоянное добавление даты)
                                        msg_text = "                    " + msg_text;
                                        msg_log = msg_log + msg_text + "\r\n";
                                    }
                                    break;
                                default:
                                    msg_log = msg_log + msg_text + "\r\n";
                            }
                        }
                    });

                    //console.log("msg_log = ", msg_log);
                    if ((msg_log !== null) && (msg_log.replace(" ", "") !== "")) {
                        t.prop('disabled', false);
                        if (gc_sess.last_msg_id > 0) {
                            console.log("append", msg_log);
                            t.val(t.prop('value') + msg_log).animate({scrollTop: t[0].scrollHeight - t[0].clientHeight}, 100);
                        }
                        else {
                            t.val(msg_log).animate({scrollTop: t[0].scrollHeight - t[0].clientHeight}, 100);
                            console.log("full read");
                        }
                        t.prop('disabled', true);
                        gc_sess.last_msg_id = last_mes_id;
                    }
                    break;

            }
        },
        error: function (xhr, status, error) {
            console.log("error: function", xhr, status, error);
            //alert('user_page.js: ' + error);
        }
    });
}

const gc_no_active_delay = 120;
const gc_interval_read_msg = 4000;
let gc_no_active = 0;
let timerId;

window.onload=function() {
    if (document.getElementById("sel_friends_list") !== null) {
        document.getElementById("sel_friends_list").addEventListener("click", choseFriend);
    };

    if (document.getElementById("send_message") !== null) {
        document.getElementById("send_message").addEventListener("click", sendMessage);
    }

    if (document.getElementById("message_text") !== null) {
        document.getElementById("message_text").addEventListener("keyup", keyUp);
        document.getElementById("message_text").addEventListener("keydown", keyDown);
    }

    document.onmousemove = activeUser;

    setInterval(function incNoActive() {
        gc_no_active++;
    }, 1000);

    timerId = setTimeout(function tick() {
            if (gc_no_active < gc_no_active_delay) {
                showMessagesLog();
            }
            timerId = setTimeout(tick, gc_interval_read_msg);
        },
        gc_interval_read_msg
    );

    initData();
    setUserData();
    showUserFriends();
};

function activeUser() {
    gc_no_active = 0;
}

function initData() {
    gc_sess.cur_user_id = localStorage.getItem('id_user');
    gc_sess.last_msg_id = -1;
    gc_sess.actual_friend_id = -1;
    gc_sess.msg_count = 0;

    let jsonData = new Object();
    jsonData.command = "0";
    serverConnectFunc(serverPath, JSON.stringify(jsonData));
}

function showUserFriends() {
    let jsonData = new Object();
    jsonData.command = "20";
    jsonData.id_user_cur = gc_sess.cur_user_id;
    serverConnectFunc(serverPath, JSON.stringify(jsonData));
};

// Чтение лога сообщений от пользователя
function showMessagesLog(i_from_user_id) {
    if (i_from_user_id === undefined) {
        i_from_user_id = gc_sess.actual_friend_id;
    }

    if ((gc_sess.cur_user_id !== null) && (gc_sess.cur_user_id !== undefined) && (i_from_user_id !== undefined)) {
        let jsonData = new Object();
        jsonData.command = "22";
        jsonData.id_user_cur = gc_sess.cur_user_id;
        jsonData.id_user_from = i_from_user_id;
        jsonData.last_message_id = gc_sess.last_msg_id;
        //console.log("showMessagesLog", JSON.stringify(jsonData));
        serverConnectFunc(serverPath, JSON.stringify(jsonData));
    }
    else {
        console.log("else showMessagesLog: cur_user_id = ", gc_sess.cur_user_id, "i_from_user_id = ", i_from_user_id);
    }
};

function choseFriend(e) {
    //console.log("choseFriend=", e.target.id, e.target.value);
    if ((e !== undefined) && (e.target !== undefined) && (e.target.id > 0)) {
        if (gc_sess.actual_friend_id !== e.target.id) {
            gc_sess.last_msg_id = -1;
            document.getElementById("message_text").value = ""
            clearMessageLog();
            showMessagesLog(e.target.id);
            gc_sess.actual_friend_id = e.target.id;
        }
    }
    else {
        //console.log("choseFriend=");
        document.getElementById("message_text").isDisabled = true;
    }
};


// Отправка сообщений
function sendMessage() {
    //console.log("sendMessage: id_user_to = ", id_user_to);

    if ((gc_sess.cur_user_id !== null) && (gc_sess.cur_user_id !== undefined)
        && (gc_sess.actual_friend_id !== null) && (gc_sess.actual_friend_id !== undefined)) {
        let jsonData = new Object();
        jsonData.command = "21";
        jsonData.id_user_from = gc_sess.cur_user_id;
        jsonData.id_user_to = gc_sess.actual_friend_id;
        jsonData.message_text = document.getElementById("message_text").value;
        document.getElementById("message_text").value = '';
        //console.log("sendMessage:", jsonData);
        serverConnectFunc(serverPath, JSON.stringify(jsonData));
        gc_sess.msg_count++;
    }
    else {
        console.log("Error occurred while trying to send message");
    }
};

function clearMessageLog() {
    let ta = $("#messages_log");

    ta.isDisabled = false;
    ta.val('');
    ta.isDisabled = true;
}

function setUserData() {
    let cookies = document.cookie;

    try {
        if (cookies !== undefined) {
            let u_name;
            u_name = get_cookie("u_name");
            if (u_name !== undefined)
                document.getElementById("id_user").innerHTML = u_name;
            else
                console.log("cookies: u_name=" + undefined);
        }
        else {
            console.log("cookies: undefined");
        }
    }
    finally {
        // console.log("cookies = " + cookies);
    }
}

function keyUp (ev) {
    let K = ev.keyCode;
    if (K === 17) keyCtrl = 0;
    else if (K === 13) keyEnter = 0;
}

function keyDown (ev) {
    let K = ev.keyCode;
    if (K === 17) keyCtrl = 1;
    else if (K === 13) keyEnter = 1;

    if ((keyCtrl === 1) && (keyEnter === 1)) sendMessage();
}