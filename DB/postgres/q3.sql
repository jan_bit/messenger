CREATE TYPE scm_mes_main.t_u2u_message_log AS (
    message_id integer,
    message_text varchar(100),
    date_create timestamp,
    readed  integer);

drop TYPE scm_mes_main.t_u2u_message_log;



do
$$
declare

    cur refcursor;
    l_id text;
    rec scm_mes_main.t_u2u_message_log;
begin
    open cur for
        select scm_mes_main.f_get_u2u_message_log(pi_user_from => 24,
                                                  pi_user_to   => 25,
                                                  pi_tms_start => to_date('21.07.2019', 'dd.mm.yyyy'));
    loop
      --fetch cur into rec;
      fetch 'name_1' into rec;
      exit when not found;

      raise notice 'l_message_id %', to_char(rec.message_id, 'FM999');
    end loop;
end;
$$;



select * from scm_mes_main.f_get_u2u_message_log(pi_user_from => 24,
		                                          pi_user_to   => 26,
		                                          pi_tms_start => to_date('29.09.2019', 'dd.mm.yyyy'),
		                                          pi_last_message_id => 200);


commit;

do
$$
declare
  l_var scm_mes_main.tp_user;
begin
  select scm_mes_main.p_try_login('anton', 'ppa') into l_var;
  raise notice 'var = %', l_var.login;
end;
$$

select scm_mes_main.p_try_login('anton', 'ppa');