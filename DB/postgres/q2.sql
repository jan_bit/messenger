
--drop FUNCTION scm_mes_main.p_add_user(pi_user_data in scm_mes_main.tp_user);
commit;

CREATE OR REPLACE FUNCTION scm_mes_main.p_add_user(
	pi_login   	   in varchar,
	pi_name        in varchar,
	pi_surname     in varchar,
	pi_patronymic  in varchar,
	pi_birth_date  in date,
	pi_gender      in integer,
	pi_email	   in varchar,
	pi_pwd_hash	   in varchar,
	pi_pwd_salt	   in varchar)
	RETURNS int
	LANGUAGE plpgsql
AS $$
    declare
      l_result integer;
	begin
		begin	  
			insert into scm_mes_main.user(login, u_name, surname, patronymic, birth_date, gender, date_create, email, pwd_hash, pwd_salt)
			values(
			  pi_login,
			  pi_name, 
			  pi_surname, 
			  pi_patronymic,
			  pi_birth_date, 
			  pi_gender, 
			  current_timestamp,
			  pi_email,
			  pi_pwd_hash,
			  pi_pwd_salt);
			 
			l_result := 0; 
		exception
		  when others then
		    begin
			    l_result := 1; 
			    perform scm_mes_log.p_add_log(
			      pi_proc_name  => 'scm_mess_main.p_add_user',
	              pi_comment    => '������ ��� ���������� ������������',
	              pi_error_text => SQLERRM);
            end;
		    RAISE exception '(%)', SQLERRM;
		end;
	
		return l_result;
	END;
$$

commit;
----------------------------------------------------------------------------------
--�����������
CREATE OR REPLACE FUNCTION scm_mes_log.p_add_log(pi_proc_name  in varchar, 
												 pi_comment    in varchar, 
												 pi_error_text in varchar) 
RETURNS void
LANGUAGE plpgsql
SECURITY DEFINER
AS $$
	declare
	  l_connect_name varchar(15);
	  l_sql_text     varchar;
	begin 
		begin
			l_connect_name := cast(ceil(random() * 100000000) as varchar);
			perform scm_extensions.dblink_connect(l_connect_name, 'host=debian-9 port=5432 dbname=db_messenger user=rl_mess_user password=rl_mess_user');
				    		   
		   l_sql_text := 
		     'insert into scm_mes_log.t_log(proc_name, comment, error_text, cur_user, ses_user)
		     values(''[PROC_NAME]'', ''[COMMENT]'', ''[ERROR_TEXT]'', ''[CUR_USER]'', ''[SES_USER]'');';
		
		    l_sql_text := replace(l_sql_text, '[PROC_NAME]',  pi_proc_name);
		    l_sql_text := replace(l_sql_text, '[COMMENT]',    pi_comment);
		    l_sql_text := replace(l_sql_text, '[ERROR_TEXT]', pi_error_text);
		    l_sql_text := replace(l_sql_text, '[CUR_USER]',   current_user);
		    l_sql_text := replace(l_sql_text, '[SES_USER]',   session_user);
			 
			perform scm_extensions.dblink_exec(l_connect_name, l_sql_text);
		    perform scm_extensions.dblink_disconnect(l_connect_name);
		exception
		  when others then
		    perform scm_extensions.dblink_disconnect(l_connect_name);
		    RAISE exception '������ ��� ����������� (% %)', l_sql_text, sqlerrm;
		END;
	
		return;
	end;
$$

----------------------------------------------------------------------------------
drop FUNCTION scm_mes_main.p_try_login(pi_login in varchar);

CREATE OR REPLACE FUNCTION scm_mes_main.p_try_login(pi_login in varchar, pi_id_user integer)
	RETURNS TABLE(id_user     integer,
				  login   	  varchar,
				  email   	  varchar,
				  u_name	  varchar,
				  surname 	  varchar,
				  patronymic  varchar,
				  gender	  integer,
				  pwd_hash    varchar,
				  pwd_salt    varchar)
LANGUAGE plpgsql 
SECURITY DEFINER
AS $$
    declare
      l_login varchar(100) := pi_login;
      l_id_user integer := pi_id_user;
	BEGIN
		begin
			l_login := replace(l_login, ' ', '');		   		    
			if l_login = '' then
				l_login := null;
			end if;
		
			if l_id_user <= 0 then
				l_id_user := null;
			end if;
		  
		   
		   if (l_login is null) and (l_id_user is null) then
		     return;
		   end if;
		    
			return query
				select u.id_user, 
					   u.login,
					   u.email,
					   u.u_name, 
					   u.surname,
					   u.patronymic,
					   u.gender,
					   u.pwd_hash,
					   u.pwd_salt
				  from scm_mes_main.user u
				 where (u.login = case when l_login is not null then l_login else u.login end)
				   and (u.id_user = coalesce(l_id_user, u.id_user))
				  limit 1;
			 
		exception
		  when others then		  
		    perform scm_mes_log.p_add_log(
		      pi_proc_name  => 'scm_mess_main.p_try_login',
              pi_comment    => '������ ��� �������� ������� ������������',
              pi_error_text => SQLERRM);
		    RAISE exception '(%)', SQLERRM;
		END;	
	END;
$$

commit;
-----------------------------------------------------
-----------------------------------------------------
drop FUNCTION scm_mes_main.f_change_password(pi_user in integer,
										      pi_new_hash   in varchar,	
										      pi_new_salt in varchar);

CREATE OR REPLACE FUNCTION scm_mes_main.f_change_password(pi_id_user in integer,
													      pi_new_hash   in varchar,	
													      pi_new_salt in varchar)    												  
	RETURNS integer
	LANGUAGE plpgsql
AS $$
	declare
	  l_result integer;
	  l_rows_count bigint;
	begin
		begin
			update scm_mes_main."user" set
			  pwd_hash = pi_new_hash,
			  pwd_salt = pi_new_salt
			where id_user = pi_id_user;
		
		    get diagnostics l_rows_count = row_count;
		   
			if l_rows_count = 1 then		
				l_result := 0; --OK
			else
				l_result := 1; --error
			    perform scm_mes_log.p_add_log(
			      pi_proc_name  => 'scm_mess_main.f_change_password',
	              pi_comment    => '��� ��������� ������ ������������ �� ������	',
	              pi_error_text => concat('pi_id_user = ', pi_id_user, ', pi_new_hash = ', pi_new_hash, ', pi_new_salt = ', pi_new_salt));				
			end if;			
		exception		  
		  when others then		  
		    perform scm_mes_log.p_add_log(
		      pi_proc_name  => 'scm_mess_main.f_change_password',
              pi_comment    => '������ ��� ��������� ������',
              pi_error_text => SQLERRM);
		    RAISE exception '(%)', SQLERRM;
		end;
	
		return l_result;
	END;
$$

commit;
-----------------------------------------------------
-----------------------------------------------------

drop view scm_mes_main.my_friends;

create or replace view scm_mes_main.v_my_friends as
select 
  u.id_user, 
  u.u_name || ' ' || u.surname as user_name
  from scm_mes_main.user u;
commit; 
------------------------------------------------------

--���� ��������
--
--0: OK
CREATE OR REPLACE FUNCTION scm_mes_main.f_send_message(pi_user_from in integer,
													   pi_user_to   in integer,	
													   pi_message_text in text)    												  
	RETURNS integer
	LANGUAGE plpgsql
AS $$
	declare
	  l_result integer;
	  new_message_id integer;	
	begin
		begin
			insert into scm_mes_main.message(id_user_post, message_text, date_create)
			values(pi_user_from, pi_message_text, current_timestamp)
			returning message_id into new_message_id;
		
			insert into scm_mes_main.mess_recip(message_id, to_user_id)
			values(new_message_id, pi_user_to);
		
			l_result := 0; --OK
		exception		  
		  when others then		  
		    perform scm_mes_log.p_add_log(
		      pi_proc_name  => 'scm_mess_main.f_send_message',
              pi_comment    => '������ ��� �������� ���������',
              pi_error_text => SQLERRM);
		    RAISE exception '(%)', SQLERRM;
		end;
	
		return l_result;
	END;
$$

commit;
-------------------------------------------------------
drop FUNCTION scm_mes_main.f_get_u2u_message_log(pi_user_from in integer,
										          pi_user_to   in integer,
										          pi_tms_start in timestamp);

--������ ������ ��������� �� ���������� ����������� ������� � ����
CREATE OR REPLACE FUNCTION scm_mes_main.f_get_u2u_message_log(pi_user_from in integer,
													          pi_user_to   in integer,
													          pi_tms_start in timestamp)    												  
	RETURNS TABLE(msg_id integer, 
				  msg_text text,  
				  readed varchar)
	LANGUAGE plpgsql
AS $$
	begin
		return query
			WITH src AS 
			    (SELECT m.message_id AS msg_id,
			         To_char(m.date_create,
			         'dd') AS msg_day,
			        CASE To_char(m.date_create, 'mm')
			        WHEN '01' THEN '������'
			        WHEN '02' THEN '�������'
			        WHEN '03' THEN '�����'
			        WHEN '04' THEN '������'
			        WHEN '05' THEN '���'
			        WHEN '06' THEN '����'
			        WHEN '07' THEN '����'
			        WHEN '08' THEN '�������'
			        WHEN '09' THEN '��������'
			        WHEN '10' THEN '�������'
			        WHEN '11' THEN '������'
			        WHEN '12' THEN '�������'
			        END AS msg_month, uf.u_name || ' ' || To_char(m.date_create, 'hh24:mi') || ' ' || e'\n' || ' ' || m.message_text AS msg_text,
			        CASE COALESCE(mr.readed, 0)
			        WHEN 0 THEN
			        '���'
			        ELSE '��'
			        END AS readed
			    FROM scm_mes_main.message m
			    JOIN scm_mes_main.mess_recip mr
			        ON mr.message_id = m.message_id
			            AND m.id_user_post in (pi_user_from, pi_user_to)
			            AND mr.to_user_id in (pi_user_from, pi_user_to)
			            AND m.date_create >= pi_tms_start
			            AND m.date_annul IS NULL
			    JOIN scm_mes_main."user" uf
			        ON uf.id_user = m.id_user_post
			    ORDER BY  m.date_create)
			    
			SELECT res.msg_id,
			       cast(res.msg_text as text) as msg_text,
			       cast(res.readed as varchar) as readed
			FROM 
			    (SELECT t3.msg_id,
			         t3.msg_text,
			         t3.readed,
			         t3.flag
			    FROM 
			        (SELECT t2.msg_id,
					        t2.msg_text,
					        t2.readed,
					        t2.flag
			          FROM 
			            (SELECT t.msg_id,
			         			cast(rpad(' ', 20, ' ') || t.msg_day || ' ' || t.msg_month AS text) AS msg_text, 
			         			cast(NULL AS varchar) AS readed, 
			         			0 AS flag, row_number() OVER(partition BY t.msg_day, t.msg_month ORDER BY  t.msg_id) AS rn
			               FROM src t) t2
			              WHERE t2.rn = 1) t3
			            UNION all 
			            SELECT t.msg_id,
			            	   cast(t.msg_text AS text) AS msg_text, 
			            	   cast(t.readed AS varchar) AS readed, 
			            	   1 AS flag
			            FROM src t ) res
			 ORDER BY  res.msg_id, res.flag;
	END;
$$

commit;
-------------------------------------------------------    


