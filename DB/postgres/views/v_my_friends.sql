CREATE OR REPLACE VIEW scm_mes_main.v_my_friends
AS SELECT u.id_user,
    (u.u_name::text || ' '::text) || u.surname::text AS user_name
   FROM scm_mes_main."user" u;

-- Permissions

ALTER TABLE scm_mes_main.v_my_friends OWNER TO rl_mess_adm;
GRANT ALL ON TABLE scm_mes_main.v_my_friends TO rl_mess_adm;
GRANT SELECT ON TABLE scm_mes_main.v_my_friends TO rl_mess_user;

comment on view scm_mes_main.v_my_friends is '������ ������';
--
comment on column scm_mes_main.v_my_friends.id_user is 'id �����';
comment on column scm_mes_main.v_my_friends.user_name is '��� �����';