grant select, insert on table scm_mes_main.message to rl_mess_user;
grant select, insert on table scm_mes_main.mess_recip to rl_mess_user;
grant select, insert on table scm_mes_log.t_log to rl_mess_user;
GRANT EXECUTE ON FUNCTION scm_mes_main.p_try_login(varchar,varchar) to rl_mess_user;
GRANT EXECUTE ON FUNCTION scm_mes_main.f_send_message(int4,int4,text) TO rl_mess_user;
grant usage on sequence scm_mes_main.message_key_seq to rl_mess_user;
grant usage on sequence scm_mes_main.mess_recip_key_seq to rl_mess_user;