-- DROP TYPE scm_mes_main.t_u2u_message_log;

CREATE TYPE scm_mes_main.t_u2u_message_log AS (
	message_id int4,
	message_text varchar(100),
	date_create timestamp,
	readed int4);