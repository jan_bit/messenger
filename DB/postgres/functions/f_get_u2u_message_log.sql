/*commit;
drop FUNCTION scm_mes_main.f_get_u2u_message_log(
  pi_user_from integer, 
  pi_user_to integer, 
  pi_tms_start timestamp without time zone,
  pi_last_message_id integer);*/
--commit;
CREATE OR REPLACE FUNCTION scm_mes_main.f_get_u2u_message_log(
    pi_user_from       integer, 
    pi_user_to 		   integer, 
    pi_tms_start       timestamp without time zone,
    pi_last_message_id integer)
 RETURNS TABLE(msg_id integer, msg_text text, msg_type integer, readed character varying)
 LANGUAGE plpgsql
AS $function$
	begin
		return query
			WITH src AS 
			    (SELECT m.message_id AS msg_id,
			         To_char(m.date_create, 'dd') AS msg_day,
			        CASE To_char(m.date_create, 'mm')
				        WHEN '01' THEN '������'
				        WHEN '02' THEN '�������'
				        WHEN '03' THEN '�����'
				        WHEN '04' THEN '������'
				        WHEN '05' THEN '���'
				        WHEN '06' THEN '����'
				        WHEN '07' THEN '����'
				        WHEN '08' THEN '�������'
				        WHEN '09' THEN '��������'
				        WHEN '10' THEN '�������'
				        WHEN '11' THEN '������'
				        WHEN '12' THEN '�������'
			        END AS msg_month, 
			        uf.u_name || ' ' || To_char(m.date_create, 'hh24:mi') || ' ' || e'\n' || ' ' || m.message_text || e'\n' AS msg_text,
			        mr.readed as readed
			    FROM scm_mes_main.message m
			    JOIN scm_mes_main.mess_recip mr
			        ON mr.message_id = m.message_id
			            AND m.id_user_post in (pi_user_from, pi_user_to)
			            AND mr.to_user_id in (pi_user_from, pi_user_to)
			            AND m.date_create >= pi_tms_start
			            AND m.date_annul IS NULL
			    JOIN scm_mes_main."user" uf
			        ON uf.id_user = m.id_user_post
			   WHERE ((coalesce(pi_last_message_id, 0) <= 0) OR (m.message_id > pi_last_message_id)) --������ ��� ��������� ��� ������� � ���������� ������������
			   ORDER BY  m.date_create)
			    
			SELECT res.msg_id,
			       cast(res.msg_text as text) as msg_text,
			       res.msg_type,
			       cast(res.readed as varchar) as readed
			FROM 
			    (SELECT t3.msg_id,
			         t3.msg_text,
			         t3.readed,
			         t3.msg_type
			    FROM 
			        (SELECT t2.msg_id,
					        t2.msg_text,
					        t2.readed,
					        t2.msg_type
			          FROM 
			            (SELECT t.msg_id,
			         			cast(t.msg_day || ' ' || t.msg_month AS text) AS msg_text, 
			         			cast(NULL AS varchar) AS readed, 
			         			1 AS msg_type, 
			         			row_number() OVER(partition BY t.msg_day, t.msg_month ORDER BY  t.msg_id) AS rn
			               FROM src t) t2
			              WHERE t2.rn = 1) t3
			            UNION all 
			            SELECT t.msg_id,
			            	   cast(t.msg_text AS text) AS msg_text, 
			            	   cast(t.readed AS varchar) AS readed, 
			            	   9 AS msg_type
			            FROM src t ) res
			 ORDER BY res.msg_id, res.msg_type;
	END;
$function$
;
