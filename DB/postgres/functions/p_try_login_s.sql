CREATE OR REPLACE FUNCTION scm_mes_main.p_try_login(pi_login character varying, pi_password character varying)
 RETURNS TABLE(id_user integer, login character varying, u_name character varying, surname character varying, gender integer)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
	BEGIN
		begin
			return query
				select u.id_user, 
					   u.login,
					   u.u_name, 
					   u.surname,  
					   u.gender			   
				  from scm_mes_main.user u
				 where u.login = replace(pi_login, ' ', '')
				   and u.password = replace(pi_password, ' ', '');
			 
		exception
		  when others then		  
		    perform scm_mes_log.p_add_log(
		      pi_proc_name  => 'scm_mess_main.p_try_login',
              pi_comment    => '������ ��� �������� ������� ������������',
              pi_error_text => SQLERRM);
		    RAISE exception '(%)', SQLERRM;
		END;	
	END;
$function$
;
