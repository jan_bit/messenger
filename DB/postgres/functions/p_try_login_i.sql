CREATE OR REPLACE FUNCTION scm_mes_main.p_try_login(pi_login character varying, pi_id_user integer)
 RETURNS TABLE(id_user integer, login character varying, email character varying, u_name character varying, surname character varying, patronymic character varying, gender integer, pwd_hash character varying, pwd_salt character varying)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
    declare
      l_login varchar(100) := pi_login;
      l_id_user integer := pi_id_user;
	BEGIN
		begin
			l_login := replace(l_login, ' ', '');		   		    
			if l_login = '' then
				l_login := null;
			end if;
		
			if l_id_user <= 0 then
				l_id_user := null;
			end if;
		  
		   
		   if (l_login is null) and (l_id_user is null) then
		     return;
		   end if;
		    
			return query
				select u.id_user, 
					   u.login,
					   u.email,
					   u.u_name, 
					   u.surname,
					   u.patronymic,
					   u.gender,
					   u.pwd_hash,
					   u.pwd_salt
				  from scm_mes_main.user u
				 where (u.login = case when l_login is not null then l_login else u.login end)
				   and (u.id_user = coalesce(l_id_user, u.id_user))
				  limit 1;
			 
		exception
		  when others then		  
		    perform scm_mes_log.p_add_log(
		      pi_proc_name  => 'scm_mess_main.p_try_login',
              pi_comment    => '������ ��� �������� ������� ������������',
              pi_error_text => SQLERRM);
		    RAISE exception '(%)', SQLERRM;
		END;	
	END;
$function$
;
