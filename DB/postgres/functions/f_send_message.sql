CREATE OR REPLACE FUNCTION scm_mes_main.f_send_message(pi_user_from integer, pi_user_to integer, pi_message_text text)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
	declare
	  l_result integer;
	  new_message_id integer;	
	begin
		begin
			insert into scm_mes_main.message(id_user_post, message_text, date_create)
			values(pi_user_from, pi_message_text, current_timestamp)
			returning message_id into new_message_id;
		
			insert into scm_mes_main.mess_recip(message_id, to_user_id)
			values(new_message_id, pi_user_to);
		
			l_result := 0; --OK
		exception		  
		  when others then		  
		    perform scm_mes_log.p_add_log(
		      pi_proc_name  => 'scm_mess_main.f_send_message',
              pi_comment    => '������ ��� �������� ���������',
              pi_error_text => SQLERRM);
		    RAISE exception '(%)', SQLERRM;
		end;
	
		return l_result;
	END;
$function$
;
