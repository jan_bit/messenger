CREATE OR REPLACE FUNCTION scm_mes_main.f_change_password(pi_id_user integer, pi_new_hash character varying, pi_new_salt character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
	declare
	  l_result integer;
	  l_rows_count bigint;
	begin
		begin
			update scm_mes_main."user" set
			  pwd_hash = pi_new_hash,
			  pwd_salt = pi_new_salt
			where id_user = pi_id_user;
		
		    get diagnostics l_rows_count = row_count;
		   
			if l_rows_count = 1 then		
				l_result := 0; --OK
			else
				l_result := 1; --error
			    perform scm_mes_log.p_add_log(
			      pi_proc_name  => 'scm_mess_main.f_change_password',
	              pi_comment    => '��� ��������� ������ ������������ �� ������	',
	              pi_error_text => concat('pi_id_user = ', pi_id_user, ', pi_new_hash = ', pi_new_hash, ', pi_new_salt = ', pi_new_salt));				
			end if;			
		exception		  
		  when others then		  
		    perform scm_mes_log.p_add_log(
		      pi_proc_name  => 'scm_mess_main.f_change_password',
              pi_comment    => '������ ��� ��������� ������',
              pi_error_text => SQLERRM);
		    RAISE exception '(%)', SQLERRM;
		end;
	
		return l_result;
	END;
$function$
;
