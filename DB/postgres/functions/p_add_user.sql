CREATE OR REPLACE FUNCTION scm_mes_main.p_add_user(pi_login character varying, pi_name character varying, pi_surname character varying, pi_patronymic character varying, pi_birth_date date, pi_gender integer, pi_email character varying, pi_pwd_hash character varying, pi_pwd_salt character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    declare
      l_result integer;
	begin
		begin	  
			insert into scm_mes_main.user(login, u_name, surname, patronymic, birth_date, gender, date_create, email, pwd_hash, pwd_salt)
			values(
			  pi_login,
			  pi_name, 
			  pi_surname, 
			  pi_patronymic,
			  pi_birth_date, 
			  pi_gender, 
			  current_timestamp,
			  pi_email,
			  pi_pwd_hash,
			  pi_pwd_salt);
			 
			l_result := 0; 
		exception
		  when others then
		    begin
			    l_result := 1; 
			    perform scm_mes_log.p_add_log(
			      pi_proc_name  => 'scm_mess_main.p_add_user',
	              pi_comment    => '������ ��� ���������� ������������',
	              pi_error_text => SQLERRM);
            end;
		    RAISE exception '(%)', SQLERRM;
		end;
	
		return l_result;
	END;
$function$
;
