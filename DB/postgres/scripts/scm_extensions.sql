create schema scm_extensions;

-- make sure everybody can use everything in the extensions schema
grant usage on schema scm_extensions to public;
grant execute on all functions in schema scm_extensions to public;

-- include future extensions
alter default privileges in schema scm_extensions
   grant execute on functions to public;

alter default privileges in schema scm_extensions
   grant usage on types to public;
   
set search_path = scm_extensions, extensions;

create extension dblink schema scm_extensions;  