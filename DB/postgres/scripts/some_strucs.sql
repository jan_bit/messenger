CREATE TABLESPACE tbs_messenger LOCATION '/var/lib/postgresql/9.6/main/base/tbs';
ALTER DATABASE db_messanger RENAME TO db_messenger;
ALTER DATABASE db_messenger SET TABLESPACE tbs_messenger;

ALTER DATABASE db_messenger OWNER TO rl_mess_adm;
ALTER schema scm_mes_main OWNER TO rl_mess_adm;
ALTER schema scm_mes_log OWNER TO rl_mess_adm;

GRANT CONNECT ON DATABASE messager TO rl_mess_user;
GRANT CONNECT ON DATABASE messager TO rl_mess_adm;

revoke all on schema scm_mess_main from rl_mess_user;
grant all privileges on scm_mes_main.gender to rl_mess_adm;
 
grant usage on schema scm_mes_main to rl_mess_user;
grant usage on schema scm_mes_log to rl_mess_user;

grant create procedure on scm_mes_log to rl_mess_adm; 

grant select, insert, delete, update on scm_mess_main.user to rl_mess_user;
grant select, insert, delete, update on scm_mess_main.gender to rl_mess_user;
grant select on scm_mes_main.v_my_friends to rl_mess_user;
grant select on scm_mes_main.my_friends to rl_mess_user;

set search_path = scm_mes_log, extensions;
set search_path = scm_mes_main, extensions;

create extension dblink schema scm_mes_log;

grant execute on dblink_connect to rl_mess_adm;

grant usage on sequence scm_mes_main.seq_user_key to rl_mess_user;
grant usage on sequence scm_mes_log.t_log_key_seq to rl_mess_user;