alter table scm_mes_main.message add constraint pk_message primary key (key);
alter table scm_mes_main.mess_recip add constraint fk_message_key foreign key (message_id) references scm_mes_main.message(message_id);
alter table scm_mes_main.mess_recip add constraint fk_to_user_id foreign key (to_user_id) references scm_mes_main.user(id_user);
alter table scm_mess_main.user add constraint fk_user_gender foreign key (gender) references scm_mess_main.gender(key);